; This gcode was written for use with Quiver DualExtruder Test Stand
; Use of this gcode outside of an identical set up may result in unexpected behaviour or damage
G21                          ; set units to millimetres
M107                         ; disable FAN 1
G90                          ; absolute positioning
M82                          ; set extruder to absolute mode
G92 E0                       ; set extruder position to 0
M400			     ; clear errors
M75			     ; start print timer
T0
M117 Touch PROBE to Noz 1    ; display message
M226 P31 S1		     ; wait for probe pin state change
T1			     ; select T1
M117 Touch PROBE to Noz 2    ; display message
M226 P31 S1		     ; wait for probe pin state change
T0			     ; select T0
M104 S260 T0		     ; set T0 temp to 260C
M104 S260 T1		     ; set T1 temp to 260C
M300 S440 P1		     ; play sound
M117 FAN 40 PERCENT          ; display message
M106 S102		     ; fan on 40 percent
G4 S10			     ; wait
M107			     ; turn fan off
G4 S5			     ; wait
M300 S440 P1		     ; play sound
M117 FAN 100 PERCENT         ; display message
M106 S255		     ; fan on a hundy P buddy
G4 S10			     ; wait
M300 S440 P5		     ; play sound
M117 FAN OFF                 ; display message
M107			     ; turn fan off
G4 S1			     ; wait a tick

M109 R260 T0		     ; wait for T0 to reach 260C
M109 R260 T1		     ; wait for T1 to reach 260C
M300 S440 P2		     ; play sound
M117 WATCH E1 MOTOR          ; display message
G4 S3			     ; wait for it
G92 E0			     ; set extruder position to 0
M117 E1 Extrude              ; display message
G1 E50 F100		     ; extrude 50mm from E1
G4 S1			     ; wait
M300 S440 P1		     ; play sound
M117 E1 Retract              ; display message
G1 E0 F100		     ; retract E1 to 0
T1			     ; select T1
M300 S440 P2		     ; play sound
M117 WATCH E2 MOTOR          ; display message
G4 S3			     ; wait
G92 E0			     ; set extruder position to 0
M117 E2 Extrude              ; display message
G1 E50 F100		     ; extrude 50mm from E2
G4 S1			     ; wait
M300 S440 P1		     ; play sound
M117 E2 Retract              ; display message
G1 E0 F100		     ; retract E2 to 0
T0			     ; select T0
M106 S255		     ; fan on a hundy P bud
M104 S70 T1		     ; set T1 temp to 70C
M109 R70		     ; wait for T0 to reach 70C 
M109 R70 T1		     ; wait for T1 to reach 70C
M104 S0 T0		     ; turn T0 heat off
M104 S0 T1		     ; turn T1 heat off
M107			     ; turn fan off
M77			     ; end print timer
M117 Test Complete           ; display message



