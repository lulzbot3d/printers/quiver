/************* *******
 *  T85_Fil_Test.ino *
 *********************/

/****************************************************************************
 *   Written By Mark Pelletier 2019 - Aleph Objects, Inc.                   *
 *                                                                          *
 *   This program is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by   *
 *   the Free Software Foundation, either version 3 of the License, or      *
 *   (at your option) any later version.                                    *
 *                                                                          *
 *   This program is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of         *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
 *   GNU General Public License for more details.                           *
 *                                                                          *
 *   To view a copy of the GNU General Public License, go to the following  *
 *   location: <http://www.gnu.org/licenses/>.                              *
 ****************************************************************************/

/****************************************************************************
 * TEST OUTPUT STATES                                                       *
 *                                                                          *
 * STATE      Description                                                   *
 *                                                                          *
 *   0        E1 and E2 Off                                                 *
 *                                                                          *
 *   1        E1 Pulsing, E2 Off                                            *
 *                                                                          *
 *   2        E1 Off, E2 Pulsing                                            *
 *                                                                          *
 *   3        E1 and E2 Pulsing                                             *
 *                                                                          *
 *   4        E1 and E2 Alternating Pulses                                  *
 *                                                                          *
 *   5        E1 and E2 On                                                  *
 *                                                                          *
 ****************************************************************************/


// Global Variables

uint8_t Fil_Test_State = 0;

uint8_t On_nOff = 0;

uint16_t T0_Delay_Count;


// Key Scan Variables

unsigned char Key_Buf = 0;
unsigned char Last_Key = 0xFF;
unsigned char Key_Value = 0;
unsigned char Key_On_Timer = 0;


ISR(TIMER0_COMPA_vect)              // This ISR fires every 1 millisecond.
{
  Key_Scan();

  State_Machine();
  
  if(++T0_Delay_Count > 250)
  {
    T0_Delay_Count = 0;

    if(On_nOff)
    {
      On_nOff = 0;
    }
    else
    {
      On_nOff = 1;
    }
  }
}


void Key_Scan (void)
{
  Key_Buf = (PINB & 0x01);
  Key_Buf |= 0xFE;
 
  if((Key_Buf != Last_Key) && (Key_On_Timer == 0))
  {
    Key_On_Timer = 10;
  }

  if(Key_On_Timer > 0)
  {
    Key_On_Timer--;
  }
  
  if(Key_On_Timer == 1)
  {
    Key_On_Timer = 0;
    
    if(Key_Buf != Last_Key)
    {
      Last_Key = Key_Buf;
      Key_Value = (~Last_Key);
    }
    else
    {
      Key_Value = 0;
    }
  }
  else
  {
    Key_Value = 0;
  }
  
  
  // KEY FUNCTIONS - 0x01 = Increment Filament Test State
  
  if(Key_Value == 0x01)
  {
    if(++Fil_Test_State > 5)
    {
      Fil_Test_State = 0;
    }
  }
  
}


void setup()
{
  DDRB = 0x1A;

  // Initialize Timer 0 as CTC

  noInterrupts();

  TCCR0A = 0x80;              // Normal Mode - OC0A Clear on Match
  TCCR0B = 0x03;              // Divide 1 MHz Clock by 256
  TCNT0  = 0;

  OCR0A  = 0x4F;              // Set for 100 kHz
  OCR0B  = 0;

  // Set Interrupt Mask for OCR0A Match

  TIMSK = 0x10;

  interrupts();
}


void State_Machine (void)
{
  if(Fil_Test_State == 0)                       // Both E1 and E2 Off
  {
    PORTB &= 0xE7;
  }
  else if(Fil_Test_State == 1)                  // E1 Pulsing, E2 Off
  {
    PORTB &= 0xEF;

    if(On_nOff)
    {
      PORTB |= 0x08;
    }
    else
    {
      PORTB &= 0xF7;
    }
    
  }
  else if(Fil_Test_State == 2)                  // E2 Pulsing, E1 Off
  {
    PORTB &= 0xF7;

    if(On_nOff)
    {
      PORTB |= 0x10;
    }
    else
    {
      PORTB &= 0xEF;
    }
  }
  else if(Fil_Test_State == 3)                  // E1 and E2 Pulsing Together
  {
    if(On_nOff)
    {
      PORTB |= 0x18;
    }
    else
    {
      PORTB &= 0xE7;
    }
  }
  else if(Fil_Test_State == 4)                  // E1 and E2 Alternating Pulses
  {
    if(On_nOff)
    {
      PORTB |= 0x10;
      PORTB &= 0xF7;
      
    }
    else
    {
      PORTB |= 0x08;
      PORTB &= 0xEF;
    }
  }
  else                                          // Both E1 and E2 On
  {
    PORTB |= 0x18;
  }
}


void loop()
{

}
