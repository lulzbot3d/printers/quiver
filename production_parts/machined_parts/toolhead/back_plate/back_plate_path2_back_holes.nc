(Exported by FreeCAD)
(Post Processor: linuxcnc_post)
(Output Time:2018-05-14 15:10:05.668286)
(begin preamble)
G17 G54 G40 G49 G80 G90
G21
(begin operation: T11: 3mm drill)
(machine: not set, mm/min)
(T11: 3mm drill)
M6 T11 G43 H11
M3 S800
M8
(finish operation: T11: 3mm drill)
(begin operation: Drilling)
(machine: not set, mm/min)
(Drilling)
(Begin Drilling)
G0 Z5.000
G90
G98
G83 X3.408 Y-18.626 Z-3.000 F120.000 Q1.000 R3.000
G83 X3.408 Y-31.086 Z-3.000 F120.000 Q1.000 R3.000
G80
G0 Z5.000
(finish operation: Drilling)
(begin postamble)
M05
G17 G54 G90 G80 G40
M2
