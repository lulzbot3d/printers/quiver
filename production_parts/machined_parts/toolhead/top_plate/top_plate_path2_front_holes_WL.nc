(Exported by FreeCAD)
(Post Processor: linuxcnc_post)
(Output Time:2018-05-15 08:00:24.246544)
(begin preamble)
G17 G54 G40 G49 G80 G90
G21
(begin operation: T10: 2.35mm drill)
(machine: not set, mm/min)
(T10: 2.35mm drill)
M6 T10 G43 H10
M3 S800
M8
(finish operation: T10: 2.35mm drill)
(begin operation: Drilling)
(machine: not set, mm/min)
(Drilling)
(Begin Drilling)
G0 Z5.000
G90
G98
G83 X21.334 Y-3.408 Z-6.000 F120.000 Q1.000 R3.000
G83 X33.791 Y-3.408 Z-6.000 F120.000 Q1.000 R3.000
G80
G0 Z5.000
(finish operation: Drilling)
(begin operation: T11: 3mm drill)
(machine: not set, mm/min)
(T11: 3mm drill)
M6 T11 G43 H11
M3 S800
(finish operation: T11: 3mm drill)
(begin operation: Drilling001)
(machine: not set, mm/min)
(Drilling001)
(Begin Drilling)
G0 Z5.000
G90
G98
G83 X14.470 Y-3.408 Z-12.000 F120.000 Q1.000 R3.000
G83 X41.437 Y-3.408 Z-12.000 F120.000 Q1.000 R3.000
G80
G0 Z5.000
(finish operation: Drilling001)
(begin postamble)
M05
G17 G54 G90 G80 G40
M2
