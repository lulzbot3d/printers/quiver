Here are prints used for calibrating a TAZ Pro.

The burn in is a bushing conditioning process which is meant to be run on newly assembled machines before the vernier dim test gcode.

The Vernier test gcode uses the provided profile which is based on the polylite PLA profile from cura 3.6.9 . After export, extra changes were made via text editor to add extra skirts and a an inspection step as well as some changes to reduce the cycle time of the gcode, which can be recreated by loading the provided .stls into cura 3.6.9 and using the provided profile to export a gcode to be compared to the provided gcode.


The source files and .stls are avaialbe for modification and reimplementation.
